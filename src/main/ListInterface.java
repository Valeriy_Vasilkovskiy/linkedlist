package main;

public interface ListInterface{
    void add(String element);
    void addArray (String[] arrString);
    void addStart(String element);
    void deleteElement(String element);
    void deleteAll();
    void deleteStart();
    void deleteEnd();
    void printList();
}
