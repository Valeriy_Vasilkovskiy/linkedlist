package main;

public class Main {
    public static void main(String[] args) {

        ListInterface interfaceList = new MyList();

        interfaceList.add("Sasha");
        interfaceList.printList();

        interfaceList.add("kolia");
        interfaceList.printList();

        String[] string = {"Xladik", "Dimon", "Jack"};
        interfaceList.addArray(string);
        interfaceList.printList();

        interfaceList.addStart("Roma");
        interfaceList.printList();

        interfaceList.deleteEnd();
        interfaceList.printList();

        interfaceList.deleteStart();
        interfaceList.printList();

        interfaceList.deleteAll();
        interfaceList.printList();
    }
}
