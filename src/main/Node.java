package main;

public class Node {

    private String information;
    private Node nextLink;

    public Node(String information) {
        this.information = information;
        this.nextLink = null;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public Node getNextLink() {
        return nextLink;
    }

    public void setNextLink(Node nextLink) {
        this.nextLink = nextLink;
    }
}
