package main;

public class MyList implements ListInterface {

    private Node start;
    private int size;

    public MyList() {
        start = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }


    @Override
    public void add(String element) {
        Node temp = start;
        if (temp == null) {
            start = new Node(element);
            size++;
            return;
        }
        while (temp.getNextLink() != null) {
            temp = temp.getNextLink();
        }
        temp.setNextLink(new Node(element));
        size++;
    }

    @Override
    public void addArray(String[] arrString) {
        for (String element : arrString) {
            add(element);
        }
    }

    @Override
    public void addStart(String element) {
        Node temp = start;
        if (temp == null) {
            start = new Node(element);
            size++;
        } else {
            start = new Node(element);
            start.setNextLink(temp);
            size++;
        }
    }

    @Override
    public void deleteElement(String element) {
        Node temp = start;
        Node next = temp.getNextLink();
        if (start.getInformation().equals(element)) {
            if (size == 1) {
                start.setInformation(null);
                size--;
                return;
            }
            start.setInformation(null);
            start = start.getNextLink();
            size--;
            return;
        }
        while (next != null) {
            if (next.getInformation().equals(element)) {
                temp.setNextLink(next.getNextLink());
                size--;
                return;
            }
            temp = next;
            next = temp.getNextLink();
        }
    }

    @Override
    public void deleteAll() {
        Node temp = start;
        int _size = size;
        for (int i = 0; i != _size; i++) {
            deleteElement(temp.getInformation());
            if (temp.getNextLink() == null) {
                return;
            } else {
                temp = temp.getNextLink();
            }
        }
    }

    @Override
    public void deleteStart() {
        deleteElement(start.getInformation());
    }

    @Override
    public void deleteEnd() {
        Node temp = start;
        for (int i = 0; i != size; i++) {
            if (temp.getNextLink() == null) {
                deleteElement(temp.getInformation());
                return;
            } else {
                temp = temp.getNextLink();
            }
        }
    }

    @Override
    public void printList() {
        Node temp = start;
        if (size == 0) {
            System.out.println("This List is empty");
        } else {
            for (int i = 1; i != size + 1; i++) {
                System.out.println(i + ")" + temp.getInformation());
                temp = temp.getNextLink();
            }
        }
        System.out.println("==================");
    }
}

